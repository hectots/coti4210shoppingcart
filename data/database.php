<?php

// Global database connection id
$connection = null;

// Connects to the database server and selects the database.
// Don't call this directly, query() function calls it if the
// connection haven't been made.
function connect() {
	global $connection;
	
	// Database connection credentials
	$host = 'localhost';
	$user = 'root';
	$pass = 'password';
	$db   = 'phpshoppingcart';
	
	// Connects the database server and selects the default database
	$connection = mysqli_connect($host, $user, $pass, $db);
	
	// TODO: Check for errors when connecting to the database server
}

// Execute a query in the database.
// If there's a result, a two-dimensional array with the data is returned.
// Otherwise, an empty array is returned.
function query($query_string) {
	global $connection;
	
	// Check if the connection is alive
	if ($connection == null) {
		// ... and if not then make the connection
		connect();
	}
	
	// Loop through the results and put them in an array
	$results_table = array();
	$results = mysqli_query($connection, $query_string); // This executes the query
	if ($results) {
		if (!is_bool($results)) {
			while ($result = mysqli_fetch_assoc($results)) { // ... and this fetch the results
				$results_table[] = $result;                  // Those results are put in the array
			}
		}
	} else {
		// TODO: Log this error instead of printing it
		print mysqli_error($connection);
	}
	
	return $results_table;
}