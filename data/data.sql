-- Database

CREATE DATABASE  `phpshoppingcart` ;




-- Tables

CREATE TABLE  `phpshoppingcart`.`customers` (
	`id` VARCHAR( 45 ) NOT NULL ,
	`password` VARCHAR( 45 ) NOT NULL ,
	`first_name` VARCHAR( 45 ) NULL ,
	`last_name` VARCHAR( 45 ) NULL ,
	`address` VARCHAR( 255 ) NULL ,
	`city` VARCHAR( 45 ) NULL ,
	`zip` VARCHAR( 6 ) NULL ,
	`state` VARCHAR( 2 ) NULL ,
	`email` VARCHAR( 45 ) NULL ,
	`phone` VARCHAR( 45 ) NULL ,
	PRIMARY KEY (  `id` )
) ENGINE = INNODB ;

CREATE TABLE  `phpshoppingcart`.`products` (
	`id` INT( 45 ) NOT NULL ,
	`name` VARCHAR( 45 ) NOT NULL ,
	`detail` VARCHAR( 255 ) NOT NULL ,
	`stock` INT( 6 ) NOT NULL ,
	`price` DOUBLE( 4, 2 ) NOT NULL ,
	`picture` VARCHAR( 255 ) NOT NULL ,
	`additional_fields` VARCHAR( 255 ) NULL ,
	PRIMARY KEY (  `id` )
) ENGINE = INNODB ;

CREATE TABLE  `phpshoppingcart`.`cart` (
	`id` INT( 45 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
	`product_id` INT( 45 ) NOT NULL ,
	`product_price` DOUBLE( 4, 2 ) NOT NULL ,
	`product_quantity` INT( 6 ) NOT NULL ,
	`user_id` VARCHAR( 45 ) NOT NULL ,
	`date` DATE NOT NULL ,
	INDEX (  `product_id` ,  `user_id` ),
	FOREIGN KEY (  `product_id` ) REFERENCES  `phpshoppingcart`.`products` ( `id` ),
	FOREIGN KEY (  `user_id` ) REFERENCES  `phpshoppingcart`.`customers` ( `id` )
) ENGINE = INNODB ;

CREATE TABLE  `phpshoppingcart`.`order` (
	`id` INT( 45 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
	`user_id` VARCHAR( 45 ) NOT NULL ,
	`receiver_name` VARCHAR( 45 ) NOT NULL ,
	`address` VARCHAR( 255 ) NOT NULL ,
	`city` VARCHAR( 45 ) NOT NULL ,
	`zip` VARCHAR( 6 ) NOT NULL ,
	`state` VARCHAR( 2 ) NOT NULL ,
	`purchase_date` DATE NOT NULL ,
	INDEX (  `user_id` ),
	FOREIGN KEY (  `user_id` ) REFERENCES  `phpshoppingcart`.`customers` ( `id` )
) ENGINE = INNODB;




-- Product Data

USE  `phpshoppingcart` ;

INSERT INTO `products` (`id`, `name`, `detail`, `stock`, `price`, `picture`) VALUES
(17749, 'Jarra para agua SS (2 Qt.)', 'Jarra para agua SS (2 Qt.)', 18, 52.12, 'images/products/image025.png'),
(19896, 'Vulcan Single Deck Electronic Oven', 'Vulcan Single Deck Electronic Oven', 0, 99.99, 'images/products/image041.png'),
(21710, '12- Cup Muffin Pan', '12- Cup Muffin Pan', 6, 8.38, 'images/products/image042.png'),
(22632, 'Espatulas de alta temperatura', 'Espatulas de alta temperatura', 13, 12.23, 'images/products/image006.png'),
(22736, 'Plastic Tumblers 9oz- 500 ct', 'Plastic Tumblers 9oz- 500 ct', 5, 49.12, 'images/products/image048.png'),
(23171, 'Moledora de café', 'Moledora de café', 1, 99.99, 'images/products/image010.png'),
(23775, 'Cucharas de medir', 'Cucharas de medir', 1, 2.74, 'images/products/image028.png'),
(23791, 'Nevera 49 cu ft', 'Nevera 49 cu ft', 1, 99.99, 'images/products/image023.png'),
(23974, 'Zafacón SS 17 gal.', 'Zafacón SS 17 gal.', 14, 49.82, 'images/products/image018.png'),
(24783, 'Dispensador de toppings', 'Dispensador de toppings', 5, 99.99, 'images/products/image003.png'),
(25210, 'Mezcladora Kitchen Aid', 'Mezcladora Kitchen Aid', 14, 99.99, 'images/products/image014.png'),
(25973, 'Whisk SS', 'Whisk SS', 3, 3.68, 'images/products/image031.png'),
(26580, '24- Standard Cups Pan', '24- Standard Cups Pan', 13, 12.88, 'images/products/image043.png'),
(28516, 'Rubbermaid Brute Round Dolly', 'Rubbermaid Brute Round Dolly', 1, 50.71, 'images/products/image039.png'),
(28674, 'Rubbermaid 32 gal. Trash Can', 'Rubbermaid 32 gal. Trash Can', 19, 29.98, 'images/products/image037.png'),
(28901, 'Microhondas Comercial', 'Microhondas Comercial', 13, 99.99, 'images/products/image036.png'),
(29216, 'Rubbermaid Storage Container 6 Qt', 'Rubbermaid Storage Container 6 Qt', 18, 4.53, 'images/products/image044.png'),
(30318, 'Mesa SS 30w*72L*36H', 'Mesa SS 30w*72L*36H', 19, 99.99, 'images/products/image024.png'),
(30370, 'Mesas Stainless Steel 49 (con ruedas)"', 'Mesas Stainless Steel 49 (con ruedas)"', 0, 99.99, 'images/products/image017.png'),
(31126, 'Taza para medir líquido (1 quart)', 'Taza para medir lí­quido (1 quart)', 13, 9.88, 'images/products/image029.png'),
(31236, 'Calentador de chocolate 2', 'Calentador de chocolate 2', 12, 99.99, 'images/products/image020.png'),
(31436, 'Fondue Machine ', 'Fondue Machine ', 15, 32.99, 'images/products/image050.png'),
(33843, 'Napkin dispenser ', 'Napkin dispenser ', 4, 8.52, 'images/products/image049.png'),
(34170, 'Plastic Dessert Plates-288 paq', 'Plastic Dessert Plates-288 paq', 2, 49.26, 'images/products/image046.png'),
(34214, 'Guantes', 'Guantes', 2, 7.32, 'images/products/image007.png'),
(35106, 'Mesa rectangular 29",Mesa rectangular 29""', 'Mesa rectangular 29",Mesa rectangular 29""', 13, 99.98, 'images/products/image015.png'),
(35867, 'Salero (48 unidades)', 'Salero (48 unidades)', 8, 15.72, 'images/products/image026.png'),
(37071, 'Scoop SS', 'Scoop SS', 0, 4.68, 'images/products/image032.png'),
(37204, 'Mixing Bowls SS', 'Mixing Bowls SS', 19, 8.32, 'images/products/image019.png'),
(40091, 'Scoop Hielo', 'Scoop Hielo', 7, 4.97, 'images/products/image033.png'),
(40721, '12 pack- Lids for 6qt containers', '12 pack- Lids for 6qt containers', 12, 47.88, 'images/products/image045.png'),
(41427, 'Ice Maker', 'Ice Maker', 7, 99.99, 'images/products/image034.png'),
(41656, 'Plastic Dinner Plates-168 paq', 'Plastic Dinner Plates-168 paq', 7, 75.23, 'images/products/image047.png'),
(42564, 'Rubbermaid 32 gal. Trash Can Lid', 'Rubbermaid 32 gal. Trash Can Lid', 9, 37.42, 'images/products/image038.png'),
(42762, '64 oz. Water Pitcher', '64 oz. Water Pitcher', 11, 4.42, 'images/products/image040.png'),
(43153, 'Sillas altas blancas (14 unidades)', 'Sillas altas blancas (14 unidades)', 7, 99.99, 'images/products/image016.png'),
(43680, 'Panini Maker', 'Panini Maker', 17, 58.42, 'images/products/image013.png'),
(44417, 'Vitrina (mantiene calor)', 'Vitrina (mantiene calor)', 10, 99.99, 'images/products/image004.png'),
(46368, 'Cortadores de masa', 'Cortadores de masa', 14, 1.87, 'images/products/image008.png'),
(47300, 'Moldes de chocolate 35 each', 'Moldes de chocolate 35 each', 15, 59.99, 'images/products/image051.png'),
(49002, 'Estufa de gas propano', 'Estufa de gas propano', 8, 99.99, 'images/products/image001.png'),
(51616, 'Espumador de leche', 'Espumador de leche', 20, 99.99, 'images/products/image002.png'),
(52168, 'Heating Display', 'Heating Display', 9, 99.99, 'images/products/image005.png'),
(52206, 'Cafetera Bunn doble con calentador', 'Cafetera Bunn doble con calentador', 8, 99.99, 'images/products/image012.png'),
(53519, 'Fregadero SS', 'Fregadero SS', 5, 99.99, 'images/products/image022.png'),
(53583, 'Calentador de chocolate', 'Calentador de chocolate', 3, 99.99, 'images/products/image021.png'),
(53764, 'Tazas para medir seco ', 'Tazas para medir seco ', 13, 5.62, 'images/products/image030.png'),
(54625, 'Cafetera Bunn (12 cups)', 'Cafetera Bunn (12 cups)', 9, 99.99, 'images/products/image011.png'),
(56668, 'Brocha de cocina', 'Brocha de cocina', 17, 1.76, 'images/products/image009.png'),
(59169, 'Contenedor para azúcar (24 unidades)', 'Contenedor para azúcar (24 unidades)', 6, 16.74, 'images/products/image027.png'),
(62516, 'Anaquel 48W*24"D*72"D"', 'Anaquel 48W*24"D*72"D"', 18, 99.99, 'images/products/image035.png');