<?php

	// File header.inc includes starting HTML code
	// like doctype and opening html and body tags.
	include('./templates/header.inc');

	// File database.php includes function for
	// quering the database
	include('./data/database.php');
	
	if (isset($_POST['login'])) { // The form was submitted
		// Check the database for that user
		$customer = query("SELECT `id`, `first_name`, `password` FROM `customers` WHERE `id` = '{$_POST['user_id']}'");
		
		if (!empty($customer)) { // If a result was returned
			$customer = $customer[0]; // We just need the first result
			
			// Check if the password matches
			if ($customer['password'] == $_POST['password']) {
				// If it does then create the session
				$_SESSION['user'] = array('id' => $_POST['user_id'], 'name' => $customer['first_name']);
				
				// ... and redirect the user to the products page
				print '<script type="text/javascript">window.location.href = "products.php"</script>';
			} else {
				// ... otherwise output and error message
				print '<div class="error">Wrong username or password</div>';
			}
		} else {
			// Here the user was not found on the database, output the error message
			print '<div class="error">Wrong username or password</div>';
		}
	}
?>

	<form id="login-form" action="login.php" method="post" onsubmit="return validateLogin()">
		<div class="form-item">
			<label for="login-user-id">User ID</label>
			<input name="user_id" id="login-user-id" type="text" />
		</div>
		<div class="form-item">
			<label for="login-password">Password</label>
			<input name="password" id="login-password" type="password" />
		</div>
		<div class="form-submit">
			<input name="login" id="login-login" type="submit" value="Login" />
		</div>
	</form>
	<div id="login-new-user">
		<a href="registration.php">New user</a>
	</div>
	
<?php

	// File footer.inc includes ending HTML code
	// like closing body and html tags.
	include('./templates/footer.inc');

?>