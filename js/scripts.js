// -- Navigation Functions --

// Shows an element with the specified id
function show(id) {
	var el = document.getElementById(id);
	if (el) {
		el.style.display = 'block';
	}
}

// Hides an element with the specified id
function hide(id) {
	var el = document.getElementById(id);
	if (el) {
		el.style.display = 'none';
	}
}

// Shows or hides an element depending on its state (hidden or visible)
function toggle(id) {
	var el = document.getElementById(id);
	if (el) {
		var display = el.style.display;
		
		if (display == 'none') {
			el.style.display = 'block';
		} else {
			el.style.display = 'none';
		}
	}
}

// Redirects the browser to a given URL
function goTo(section) {
	window.location.href = section;
}




// -- Form Validation --

// Validates forms based on rules passed as arguments.
// The rules argument should be an array of objects
// each containing an id, an isValid function and
// an errorMessage. The is isValid function should
// return true if the passed value is valid; false otherwise.
function validate(rules) {	
	var valid = true;
	var errors = '<ul>';
	for (var i = 0; i != rules.length; i++) {
		var rule = rules[i];
		
		// Show a message for every invalid input
		var el = document.getElementById(rule.id);
		if (el != null && !rule.isValid(el.value)) {
			valid = false;
			errors += '<li>' + rule.errorMessage + '</li>';
		}
	}
	errors += '</ul>';
	
	if (!valid) {
		// If at least one input was invalid, show the errors
		var errorMessages = document.getElementById('error-messages');
		if (errorMessages) {
			errorMessages.innerHTML = errors;
		}
		
		return false;
	} else {
		return true;
	}
}

// Validation rules for the user details and registration forms
function validateUser() {
	// Rules for a valid form
	var rules = [
		{
			id: 'user-user-id',
			isValid: function(value) {
				return value != "";
			},
			errorMessage: 'User ID is required',
		},
		
		{
			id: 'user-password',
			isValid: function(value) {
				return value != "";
			},
			errorMessage: 'Password is required',
		},
		
		{
			id: 'user-password-confirmation',
			isValid: function(value) {
				var password = document.getElementById('user-password').value;
				return value != "" && value == password;
			},
			errorMessage: 'Passwords does not match',
		},
		
		{ // First name should be a single name, a name with initial or two full names
			id: 'user-first-name',
			isValid: function(value) {
				var pattern1 = value.toString().match(/[A-Z][a-z]*/);
				var pattern2 = value.toString().match(/[A-Z][a-z]* [A-Z]/);
				var pattern3 = value.toString().match(/[A-Z][a-z]* [A-Z][a-z]*/);
				
				return value == "" || pattern1 || pattern2 || pattern3;
			},
			errorMessage: 'Invalid name (first)',
		},
		
		{ // Last name should be two names, father's lastname and mother's lastname
			id: 'user-last-name',
			isValid: function(value) {
				return value == "" || value.toString().match(/[A-Z][a-z].* [A-Z][a-z].*/);
			},
			errorMessage: 'Invalid name (last)',
		},
		
		{ // ZIP code should be between 3 and 5 digits
			id: 'user-zip',
			isValid: function(value) {
				return value == "" || value.toString().match(/\d{3,5}/);
			},
			errorMessage: 'Invalid ZIP code',
		},
		
		{ // Phone number should be either 555-555-5555 or (555)-555-5555 or 5555555555
			id: 'user-phone',
			isValid: function(value) {
				var pattern1 = value.toString().match(/\d{3}-\d{3}-\d{4}/);
				var pattern2 = value.toString().match(/\(\d{3}\)-\d{3}-\d{4}/);
				var pattern3 = value.toString().match(/\d{10}/);
				
				return value == "" || pattern1 || pattern2 || pattern3;
			},
			errorMessage: 'Invalid phone number',
		},
		
		{ // Email should be example@example.com
			id: 'user-email',
			isValid: function(value) {
				return value == "" || value.toString().match(/.*@.*/);
			},
			errorMessage: 'Invalid email',
		},
	];
	
	return validate(rules);
}

function validateCheckout() {
	var rules = [
		{ // Credit card must be 12 digits
			id: 'checkout-card-number',
			isValid: function(value) { return value.toString().match(/\d{12}/); },
			errorMessage: 'Invalid card number',
		},
		
		{ // Month should be between 1 and 12
			id: 'checkout-expiration-date-month',
			isValid: function(value) { return value > 0 && value < 12; },
			errorMessage: 'Invalid card expiration date (month)',
		},
		
		{ // Year should be greater than the current year
			id: 'checkout-expiration-date-year',
			isValid: function(value) {
				var now = new Date();
				return now.getFullYear() % 1000 < value;
			},
			errorMessage: 'Invalid card expiration date (year)',
		},
		
		{ // CSC should be 3 digits
			id: 'checkout-csc',
			isValid: function(value) { return value.toString().match(/\d{3}/); },
			errorMessage: 'Invalid CSC number',
		},
		
		{ // First name should be a single name, a name with initial or two full names
			id: 'checkout-first-name',
			isValid: function(value) {
				var pattern1 = value.toString().match(/[A-Z][a-z]*/);
				var pattern2 = value.toString().match(/[A-Z][a-z]* [A-Z]/);
				var pattern3 = value.toString().match(/[A-Z][a-z]* [A-Z][a-z]*/);
				
				return pattern1 || pattern2 || pattern3;
			},
			errorMessage: 'Invalid name (first)',
		},
		
		{ // Last name should be two names, father's lastname and mother's lastname
			id: 'checkout-last-name',
			isValid: function(value) {
				return value.toString().match(/[A-Z][a-z].* [A-Z][a-z].*/);
			},
			errorMessage: 'Invalid name (last)',
		},
		
		{ // ZIP code should be between 3 and 5 digits
			id: 'checkout-zip',
			isValid: function(value) {
				return value.toString().match(/\d{3,5}/);
			},
			errorMessage: 'Invalid ZIP code',
		},
		
		{ // Phone number should be either 555-555-5555 or (555)-555-5555 or 5555555555
			id: 'checkout-phone',
			isValid: function(value) {
				var pattern1 = value.toString().match(/\d{3}-\d{3}-\d{4}/);
				var pattern2 = value.toString().match(/\(\d{3}\)-\d{3}-\d{4}/);
				var pattern3 = value.toString().match(/\d{10}/);
				
				return pattern1 || pattern2 || pattern3;
			},
			errorMessage: 'Invalid phone number',
		},
		
		{ // Email should be example@example.com
			id: 'checkout-email',
			isValid: function(value) {
				return value.toString().match(/.*@.*/);
			},
			errorMessage: 'Invalid email',
		},
	];
	
	return validate(rules);
}