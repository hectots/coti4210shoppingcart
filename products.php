<?php

	// File header.inc includes starting HTML code
	// like doctype and opening html and body tags.
	include('./templates/header.inc');

	// File database.php includes function for
	// quering the database
	include('./data/database.php');
?>

	<div id="catalog" class="page">		
		<div id="catalog-products">
			<?php
			
				// Query the database for products
				$products = query("SELECT * FROM `products`");
				
				// Output catalog list table
				
				print '<form id="catalog-products-form" action="shopping_cart.php" method="post">';
				print '<table id="catalog-table">';
				print '<tr>';
				print '<th>Image</th>';
				print '<th>Product ID</th>';
				print '<th>Detail</th>';
				print '<th>Availability</th>';
				print '<th>Pricing (USD)</th>';
				print '<th>Quantity</th>';
				print '</tr>';
				
				// Print each product
				for ($i = 0; $i != count($products); $i++) {
					$product = $products[$i];
				
					print '<tr id="' . $product["id"] . '">';
					print '<td><img src="' . $product["picture"] . '" /></td>';
					print '<td>' . $product["id"] . '</td>';
					print '<td>' . $product["detail"] . '</td>';
					print '<td>' . $product["stock"] . ' items in stock</td>';
					print '<td>$' . $product["price"] . '</td>';
					print '<td><input id="catalog-quantity-' . $product["id"] . '" name="quantity_' . $product["id"] . '" type="text" /><input type="submit" value="Add to cart" name="add_' . $product["id"] . '" />';
					print '</tr>';
				}
				print '</table>';
				print '</form>';
			?>
		</div>
	</div>
	
<?php

	// File footer.inc includes ending HTML code
	// like closing body and html tags.
	include('./templates/footer.inc');

?>