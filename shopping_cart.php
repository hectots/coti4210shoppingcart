<?php

	// File header.inc includes starting HTML code
	// like doctype and opening html and body tags.
	include('./templates/header.inc');

	// File database.php includes function for
	// quering the database
	include('./data/database.php');
	
	if (isset($_SESSION['user'])) { // Require the user to be logged in
		if (isset($_POST)) { // The form was submitted or received a value
			// Loop over the received data
			foreach ($_POST as $item => $value) {
				if (!strncmp($item, 'add_', 4)) { // Check if its start with "add_"
					$id = substr($item, 4); // Removes the "add_" part
					$quantity = $_POST['quantity_' . $id]; // Gets the quantity associated with that product
				
					// Gets the price from the database
					$price = query("SELECT `price` FROM `products` WHERE `id` = '$id'");
					$price = $price[0]['price']; // We only need one value
				
					// ... and we get the user id from the session
					$user_id = $_SESSION['user']['id'];
				
					// Now we're ready to insert the data
					$query_string  = "INSERT INTO `cart` (`product_id`, `product_price`, `product_quantity`, `user_id`, `date`) ";
					$query_string .= "VALUES ('$id', $price, $quantity, '$user_id', NOW())";
				
					query($query_string);
				}
			}
		}
		
		if (isset($_GET)) {
			// Loop over the received data
			foreach ($_GET as $item => $value) {
				if ($item == "update") { // Check if the action is an update
					$id = $value;
					$quantity = $_GET['quantity_' . $id]; // Gets the quantity associated with that product
					
					// Updates the cart with the new quantity
					query("UPDATE `cart` SET `product_quantity` = $quantity WHERE `product_id` = $id");
				}
				
				if ($item == "remove") { // Check if the action is a remove
					$id = $value;
										
					// Removes the item from the cart
					query("DELETE FROM `cart` WHERE `product_id` = $id");
				}
			}
		}
	} else { // ... if is not logged in then redirect it
		// Adds a javascript that redirects to the login page
		print '<script type="text/javascript">window.location.href = "login.php"</script>';
	}
?>

	<div id="shopping-cart" class="page">		
		<div id="shopping-cart-items">
			<?php
			
				// Query the database for the content of the cart, joining it with
				// the products table to get the details.
				$products = query("SELECT p.`id`, p.`price`, p.`detail`, c.`product_quantity` FROM `cart` c, `products` p WHERE c.`product_id` = p.`id`");
				
				if (!empty($products)) {
					// Output cart content table
				
					print '<form id="shopping-cart-update-form" action="shopping_cart.php" method="get">';
					print '<table id="cart-table">';
					print '<tr>';
					print '<th>Detail</th>';
					print '<th>Price</th>';
					print '<th>Quantity</th>';
					print '<th>Total</th>';
					print '</tr>';
				
					// Print each product
					for ($i = 0; $i != count($products); $i++) {
						$product = $products[$i];
				
						print '<tr id="' . $product["id"] . '">';
						print '<td>' . $product["detail"] . '</td>';
						print '<td>$' . $product["price"] . '</td>';
						print '<td>';
						print '<input id="catalog-quantity-' . $product["id"] . '" name="quantity_' . $product["id"] . '" type="text" value="' . $product["product_quantity"] . '" />';
						print '<input id="update-"' . $product["id"] . ' name="update" type="hidden" value="' . $product["id"] . '" />';
						print '<a href="javascript:document.forms[\'shopping-cart-update-form\'].submit()">Update</a> ';
						print '<a href="shopping_cart.php?remove=' . $product["id"] . '">Remove</a>';
						print '</td>';
						print '<td>$' . ($product["price"] * $product["product_quantity"]) . '</td>';
						print '</tr>';
					}
					print '</table>';
					print '</form>';
				} else {
					print '<p>The cart is empty</p>';
				}
			?>
		</div>
		<div id="shopping-cart-controls">
			<form id="shopping-cart-controls-form">
				<input id="shopping-cart-continue-shopping" type="button" value="Continue Shopping" onclick="goTo('products.php')" />
				<input id="shopping-cart-checkout" type="button" value="Checkout" onclick="goTo('check_out.php')" />
			</form>
		</div>
	</div>
	
<?php

	// File footer.inc includes ending HTML code
	// like closing body and html tags.
	include('./templates/footer.inc');

?>