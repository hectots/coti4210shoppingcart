<?php session_start(); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Language" content="en-us" />

    <title>Final Project - Shopping Cart</title>

	<link rel="stylesheet" href="css/styles.css" type="text/css" media="all" />
</head>
<body>
	
	<!-- Login/Logout Links -->
	<div id="account-links">
		<ul>
			<li>
				<?php if(!isset($_SESSION['user'])): ?>
					<a href="login.php">Login</a>
				<?php else: ?>
					<a href="logout.php"><?php print $_SESSION['user']['name'] ?>, Log off</a>
				<?php endif; ?>
			</li>
		</ul>
	</div>
	
	<!-- Main navigation controls -->
	<div id="navigation-controls">
		<form id="navigation-controls-form">
			<input id="navigation-control-products" type="button" value="Products" onclick="goTo('products.php')" />
			<input id="navigation-control-search" type="button" value="Search" onclick="goTo('search.php')" />
			<input id="navigation-control-cart" type="button" value="Shopping Cart" onclick="goTo('shopping_cart.php')" />
			<input id="navigation-control-user" type="button" value="User Details" onclick="goTo('user_details.php')" />
			<input id="navigation-control-register" type="button" value="Registration" onclick="goTo('registration.php')" />
		</form>
	</div>