<?php

	// File header.inc includes starting HTML code
	// like doctype and opening html and body tags.
	include('./templates/header.inc');

	// File database.php includes function for
	// quering the database
	include('./data/database.php');
?>

	<div id="order-complete" class="page">
		<div id="order-complete-summary">
			<div id="order-complete-payment-total">
				You just made a payment of
				<br />
				<span id="order-complete-payment-total-amount">
					<?php

						// Query the database for the content of the cart, joining it with
						// the products table to get the details.
						$products = query("SELECT p.`id`, p.`price`, p.`detail`, c.`product_quantity` FROM `cart` c, `products` p WHERE c.`product_id` = p.`id`");

						if (!empty($products)) {

							$cart_total = 0;
							for ($i = 0; $i != count($products); $i++) {
								$product = $products[$i];
								$cart_total += $product["price"] * $product["product_quantity"];
							}
						}
						
						print $cart_total;
					?>
				</span>
			</div>
						
			<div id="order-complete-billing-info-summary">
				<h3>Shipped to</h3>
				<?php
					// Get the order's data
					$order = query("SELECT o.`id`, o.`receiver_name`, o.`address`, o.`city`, o.`state`, o.`zip`, c.`email`, c.`first_name` FROM `order` o, `customers` c WHERE o.`user_id` = c.`id` AND o.`id` = ${_POST['order_id']}");
					$order = $order[0]; // We need just the first value
				
					print '<p>' . $order['receiver_name'] . '<br />';
					print $order['address'] . '<br />';
					print $order['city'] . ', ' . $order['state'] . ' ' . $order['zip'] . '<br />';
					print /* $order['country'] .*/ '</p>';
				?>
			</div>
		</div>
		
		<div id="order-complete-thank-you-message">
			<h1>Thanks for your order</h1>
			
			<div id="order-complete-transaction-info">
				<p><strong><span id="order-complete-name"></span><?php print $order['first_name']; ?>, you just completed your payment.</strong></p>
				<p>Your transaction ID for this payment is: <span id="order-complete-transaction-id"><?php print $order['id']?></span></p>
				<p>We'll send a confirmation email to <span id="order-complete-email"><?php print $order['email']?></span></p>
			</div>
		</div>
	</div>
	
<?php

	// File footer.inc includes ending HTML code
	// like closing body and html tags.
	include('./templates/footer.inc');

?>