<?php

	// File header.inc includes starting HTML code
	// like doctype and opening html and body tags.
	include('./templates/header.inc');

	// File database.php includes function for
	// quering the database
	include('./data/database.php');
?>

	<div id="catalog" class="page">		
		<div id="catalog-search">
			<form action="search.php" method="get">
				<div class="form-item">
					<label for="catalog-search-id">Product ID</label>
					<input name="product_id" id="catalog-search-id" type="text" />
				</div>
				<div class="form-item">
					<label for="catalog-search-detail">Detail</label>
					<input name="product_detail" id="catalog-search-detail" type="text" />
				</div>
				<div class="form-submit">
					<input id="catalog-search-search-items" type="submit" value="Search Items" />
				</div>
			</form>
		</div>
		
		<div id="catalog-products">
			<?php
				if (isset($_GET['product_id']) || isset($_GET['product_detail'])) {
					// We need to query the database using the criteria enter in
					// the search box. Here we construct a WHERE clause using those
					// values and append them to the query.
					
					// This array will hold all the conditions in the search
					$conditions = array();
					
					// We have to test the existance of each condition
					
					// This condition is added if something was entered for Product ID
					if (isset($_GET['product_id']) && !empty($_GET['product_id'])) {
						$conditions[] = "WHERE `id` = {$_GET['product_id']}";
					}
					
					// This condition is added if something was entered for Product Detail
					if (isset($_GET['product_detail']) && !empty($_GET['product_detail'])) {
						$conditions[] = "WHERE `detail` LIKE '%{$_GET['product_detail']}%'";
					}
					
					// Then we join the conditions to form the WHERE clause
					$where = "";
					for ($c = 0; $c != count($conditions) - 1; $c++) {
						$where .= $conditions[$c] . " AND "; // We join each condition with an AND
					}
					$where .= $conditions[$c];
					
					// Query the database
					$products = query("SELECT * FROM `products` $where");

					// Output catalog list table

					print '<form id="catalog-products-form" action="shopping_cart.php" method="post">';
					print '<table id="catalog-table">';
					print '<tr>';
					print '<th>Image</th>';
					print '<th>Product ID</th>';
					print '<th>Detail</th>';
					print '<th>Availability</th>';
					print '<th>Pricing (USD)</th>';
					print '<th>Quantity</th>';
					print '</tr>';

					// Print each product
					for ($i = 0; $i != count($products); $i++) {
						$product = $products[$i];

						print '<tr id="' . $product["id"] . '">';
						print '<td><img src="' . $product["picture"] . '" /></td>';
						print '<td>' . $product["id"] . '</td>';
						print '<td>' . $product["detail"] . '</td>';
						print '<td>' . $product["stock"] . ' items in stock</td>';
						print '<td>$' . $product["price"] . '</td>';
						print '<td><input id="catalog-quantity-' . $product["id"] . '" name="quantity_' . $product["id"] . '" type="text" /><input type="submit" value="Add to cart" name="add_' . $product["id"] . '" />';
						print '</tr>';
					}
					print '</table>';
					print '</form>';
				}
			?>
		</div>
	</div>
	
<?php

	// File footer.inc includes ending HTML code
	// like closing body and html tags.
	include('./templates/footer.inc');

?>