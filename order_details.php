<?php

	// File header.inc includes starting HTML code
	// like doctype and opening html and body tags.
	include('./templates/header.inc');

	// File database.php includes function for
	// quering the database
	include('./data/database.php');
	
	if (isset($_SESSION['user'])) { // Require the user to be logged in
		if (isset($_POST['review_and_continue'])) { // Checkout form was submitted
			// Build the query using the data received from the checkout form
			$query_string  = "INSERT INTO `order` (`user_id`, `receiver_name`, `address`, `city`, `zip`, `state`, `purchase_date`) ";
			$query_string .= "VALUES ('{$_SESSION['user']['id']}', '{$_POST['first_name']} {$_POST['last_name']}', '{$_POST['address']}', '{$_POST['city']}', '{$_POST['zip']}', '{$_POST['state']}', NOW())";
			
			// ... and execute the query
			query($query_string);
		} else { // ... but if not, redirect
			// Adds a javascript that redirects to the login page
			print '<script type="text/javascript">window.location.href = "check_out.php"</script>';
		}
	} else { // ... if is not logged in then redirect it
		// Adds a javascript that redirects to the login page
		print '<script type="text/javascript">window.location.href = "login.php"</script>';
	}
?>

	<div id="review-and-continue" class="page">
		<div id="review-and-continue-order-summary">
			<?php
			
				// Query the database for the content of the cart, joining it with
				// the products table to get the details.
				$products = query("SELECT p.`id`, p.`price`, p.`detail`, c.`product_quantity` FROM `cart` c, `products` p WHERE c.`product_id` = p.`id`");
				
				if (!empty($products)) {
					// Output cart content table
				
					print '<form id="shopping-cart-update-form" action="shopping_cart.php" method="get">';
					print '<table id="review-cart-table">';
					print '<tr>';
					print '<th>Description</th>';
					print '<th>Total</th>';
					print '</tr>';
				
					// Print each product
					$cart_total = 0;
					for ($i = 0; $i != count($products); $i++) {
						$product = $products[$i];
				
						print '<tr id="' . $product["id"] . '">';
						print '<td>' . $product["detail"] . '<br />';
						print 'Item price: $' . $product["price"] . '<br />';
						print 'Quantity: ' . $product["product_quantity"] . '</td>';
						print '<td>$' . ($product["price"] * $product["product_quantity"]) . '</td>';
						print '</tr>';
						
						$cart_total += $product["price"] * $product["product_quantity"];
					}
					print '</table>';
					print '</form>';
				} else {
					print '<p>The cart is empty</p>';
				}
			?>
		</div>
		
		<div id="review-and-continue-info">
			<h1>Review your information</h1>
			
			<div id="review-and-continue-shipping-address">
				<h3>Shipping address</h3>
				<?php
					print '<p>' . $_POST['first_name'] . " " . $_POST['last_name'] . '<br />';
					print $_POST['address'] . '<br />';
					print $_POST['city'] . ', ' . $_POST['state'] . ' ' . $_POST['zip'] . '<br />';
					print $_POST['country'] . '</p>';
				?>
			</div>
			
			<div id="review-and-continue-payment-methods">
				<h3>Payment methods</h3>
				<?php
					print '<p>Credit/Debit Card: XXXX-XXXX-XXXX-' . substr($_POST['card_number'], strlen($_POST['card_number']) - 4, strlen($_POST['card_number']));
					print ' <span id="review-and-continue-total">$' . $cart_total . ' USD</span></p>';
				?>
			</div>
			
			<div id="review-and-continue-contact-info">
				<h3>Contact information</h3>
				<?php
					print '<p>' . $_POST['email'] . '</p>';
				?>
			</div>
			
			<div id="review-and-continue-confirm-and-pay">
				<form id="review-and-continue-confirm-and-pay-form" action="thank_you.php" method="post">
					<?php
					
						// Here we get the order's id to send it on submit
						$info = query("SELECT MAX(`id`) AS `id` FROM `order` o");
						$info = $info[0]; // We just need the first value
					
					?>
					<input id="review-and-continue-order-id" name="order_id" type="hidden" value="<?php print $info['id']; ?>" />
					<input id="review-and-continue-pay" type="submit" value="Pay Now" />
				</form>
			</div>
		</div>
	</div>
	
<?php

	// File footer.inc includes ending HTML code
	// like closing body and html tags.
	include('./templates/footer.inc');

?>