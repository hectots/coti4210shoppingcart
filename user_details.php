<?php

	// File header.inc includes starting HTML code
	// like doctype and opening html and body tags.
	include('./templates/header.inc');

	// File database.php includes function for
	// quering the database
	include('./data/database.php');
	
	if (isset($_SESSION['user'])) { // The user must be registered and logged in
		if (isset($_POST['save'])) { // The form was submitted
			// Build the query string to update the customer's data
			// TODO: Watch out! Protect this code against XSS!
			$query_string  = "UPDATE `customers` SET `first_name`='{$_POST['first_name']}', `last_name`='{$_POST['last_name']}', `address`='{$_POST['address1']}', `city`='{$_POST['city']}', `state`='{$_POST['state']}', `zip`='{$_POST['zip']}', `phone`='{$_POST['phone']}', `email`='{$_POST['email']}' ";
			$query_string .= "WHERE `id`='{$_SESSION['user']['id']}'";
							
			// Execute the query
			query($query_string);
			
			// Update session with new info
			$_SESSION['user']['name'] = $_POST['first_name'];
		}
		
		// Get the customer data from the database to fill the form with current values
		$customer = query("SELECT * FROM `customers` WHERE `id` = '{$_SESSION['user']['id']}'");
		$customer = $customer[0]; // We need just the first row
	} else { // ... if not then we redirect it
		// Adds a javascript that redirects to the registration page
		print '<script type="text/javascript">window.location.href = "registration.php"</script>';
	}
?>

	<div id="error-messages">
		<!-- The Javascript validation function will put the error messages here -->
	</div>
	<form id="user-form" action="user_details.php" method="post" onsubmit="return validateUser()">
		<div class="form-item">
			<label for="user-first-name">First name</label>
			<input name="first_name" id="user-first-name" type="text" value="<?php print $customer['first_name']; ?>" />
		</div>
		<div class="form-item">
			<label for="user-last-name">Last name</label>
			<input name="last_name" id="user-last-name" type="text" value="<?php print $customer['last_name']; ?>" />
		</div>
		<div class="form-item">
			<label for="user-address-1">Address line 1</label>
			<input name="address1" id="user-address-1" type="text" value="<?php print $customer['address']; ?>" />
		</div>
		<div class="form-item">
			<label for="user-address-2">
				Address line 2
			</label>
			<span class="note">(optional)</span>
			<input name="address2" id="user-address-2" type="text" value="" />
		</div>
		<div class="form-item">
			<label for="user-city">City/State</label>
			<input name="city" id="user-city" type="text" value="<?php print $customer['city']; ?>" />
			<select name="state" id="user-state">
				<option <?php if ($customer['state'] == 'NA') { print 'selected="selected"'; } ?> value="NA">N/A</option>
				<option <?php if ($customer['state'] == 'AL') { print 'selected="selected"'; } ?> value="AL">AL</option> 
				<option <?php if ($customer['state'] == 'AK') { print 'selected="selected"'; } ?> value="AK">AK</option> 
				<option <?php if ($customer['state'] == 'AZ') { print 'selected="selected"'; } ?> value="AZ">AZ</option> 
				<option <?php if ($customer['state'] == 'AR') { print 'selected="selected"'; } ?> value="AR">AR</option> 
				<option <?php if ($customer['state'] == 'CA') { print 'selected="selected"'; } ?> value="CA">CA</option> 
				<option <?php if ($customer['state'] == 'CO') { print 'selected="selected"'; } ?> value="CO">CO</option> 
				<option <?php if ($customer['state'] == 'CT') { print 'selected="selected"'; } ?> value="CT">CT</option> 
				<option <?php if ($customer['state'] == 'DE') { print 'selected="selected"'; } ?> value="DE">DE</option> 
				<option <?php if ($customer['state'] == 'DC') { print 'selected="selected"'; } ?> value="DC">DC</option> 
				<option <?php if ($customer['state'] == 'FL') { print 'selected="selected"'; } ?> value="FL">FL</option> 
				<option <?php if ($customer['state'] == 'GA') { print 'selected="selected"'; } ?> value="GA">GA</option> 
				<option <?php if ($customer['state'] == 'HI') { print 'selected="selected"'; } ?> value="HI">HI</option> 
				<option <?php if ($customer['state'] == 'ID') { print 'selected="selected"'; } ?> value="ID">ID</option> 
				<option <?php if ($customer['state'] == 'IL') { print 'selected="selected"'; } ?> value="IL">IL</option> 
				<option <?php if ($customer['state'] == 'IN') { print 'selected="selected"'; } ?> value="IN">IN</option> 
				<option <?php if ($customer['state'] == 'IA') { print 'selected="selected"'; } ?> value="IA">IA</option> 
				<option <?php if ($customer['state'] == 'KS') { print 'selected="selected"'; } ?> value="KS">KS</option> 
				<option <?php if ($customer['state'] == 'KY') { print 'selected="selected"'; } ?> value="KY">KY</option> 
				<option <?php if ($customer['state'] == 'LA') { print 'selected="selected"'; } ?> value="LA">LA</option> 
				<option <?php if ($customer['state'] == 'ME') { print 'selected="selected"'; } ?> value="ME">ME</option> 
				<option <?php if ($customer['state'] == 'MD') { print 'selected="selected"'; } ?> value="MD">MD</option> 
				<option <?php if ($customer['state'] == 'MA') { print 'selected="selected"'; } ?> value="MA">MA</option> 
				<option <?php if ($customer['state'] == 'MI') { print 'selected="selected"'; } ?> value="MI">MI</option> 
				<option <?php if ($customer['state'] == 'MN') { print 'selected="selected"'; } ?> value="MN">MN</option> 
				<option <?php if ($customer['state'] == 'MS') { print 'selected="selected"'; } ?> value="MS">MS</option> 
				<option <?php if ($customer['state'] == 'MO') { print 'selected="selected"'; } ?> value="MO">MO</option> 
				<option <?php if ($customer['state'] == 'MT') { print 'selected="selected"'; } ?> value="MT">MT</option> 
				<option <?php if ($customer['state'] == 'NE') { print 'selected="selected"'; } ?> value="NE">NE</option> 
				<option <?php if ($customer['state'] == 'NV') { print 'selected="selected"'; } ?> value="NV">NV</option> 
				<option <?php if ($customer['state'] == 'NH') { print 'selected="selected"'; } ?> value="NH">NH</option> 
				<option <?php if ($customer['state'] == 'NJ') { print 'selected="selected"'; } ?> value="NJ">NJ</option> 
				<option <?php if ($customer['state'] == 'NM') { print 'selected="selected"'; } ?> value="NM">NM</option> 
				<option <?php if ($customer['state'] == 'NY') { print 'selected="selected"'; } ?> value="NY">NY</option> 
				<option <?php if ($customer['state'] == 'NC') { print 'selected="selected"'; } ?> value="NC">NC</option> 
				<option <?php if ($customer['state'] == 'ND') { print 'selected="selected"'; } ?> value="ND">ND</option> 
				<option <?php if ($customer['state'] == 'OH') { print 'selected="selected"'; } ?> value="OH">OH</option> 
				<option <?php if ($customer['state'] == 'OK') { print 'selected="selected"'; } ?> value="OK">OK</option> 
				<option <?php if ($customer['state'] == 'OR') { print 'selected="selected"'; } ?> value="OR">OR</option> 
				<option <?php if ($customer['state'] == 'PA') { print 'selected="selected"'; } ?> value="PA">PA</option> 
				<option <?php if ($customer['state'] == 'PR') { print 'selected="selected"'; } ?> value="PR">PR</option> 
				<option <?php if ($customer['state'] == 'RI') { print 'selected="selected"'; } ?> value="RI">RI</option> 
				<option <?php if ($customer['state'] == 'SC') { print 'selected="selected"'; } ?> value="SC">SC</option> 
				<option <?php if ($customer['state'] == 'SD') { print 'selected="selected"'; } ?> value="SD">SD</option> 
				<option <?php if ($customer['state'] == 'TN') { print 'selected="selected"'; } ?> value="TN">TN</option> 
				<option <?php if ($customer['state'] == 'TX') { print 'selected="selected"'; } ?> value="TX">TX</option> 
				<option <?php if ($customer['state'] == 'UT') { print 'selected="selected"'; } ?> value="UT">UT</option> 
				<option <?php if ($customer['state'] == 'VT') { print 'selected="selected"'; } ?> value="VT">VT</option> 
				<option <?php if ($customer['state'] == 'VA') { print 'selected="selected"'; } ?> value="VA">VA</option> 
				<option <?php if ($customer['state'] == 'WA') { print 'selected="selected"'; } ?> value="WA">WA</option> 
				<option <?php if ($customer['state'] == 'WV') { print 'selected="selected"'; } ?> value="WV">WV</option> 
				<option <?php if ($customer['state'] == 'WI') { print 'selected="selected"'; } ?> value="WI">WI</option> 
				<option <?php if ($customer['state'] == 'WY') { print 'selected="selected"'; } ?> value="WY">WY</option>
			</select>
		</div>
		<div class="form-item">
			<label for="user-zip">Zip code</label>
			<input name="zip" id="user-zip" type="text" value="<?php print $customer['zip']; ?>" />
		</div>
		<div class="form-item">
			<label for="user-phone">Telephone</label>
			<span class="note">555-555-1234</span>
			<input name="phone" id="user-phone" type="text" value="<?php print $customer['phone']; ?>" />
		</div>
		<div class="form-item">
			<label for="user-email">Email</label>
			<input name="email" id="user-email" type="text" value="<?php print $customer['email']; ?>" />
		</div>
		<div class="form-submit">
			<input name="save" id="user-save" type="submit" value="Save" />
		</div>
	</form>
	
<?php

	// File footer.inc includes ending HTML code
	// like closing body and html tags.
	include('./templates/footer.inc');

?>