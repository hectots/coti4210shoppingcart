<?php

	// File header.inc includes starting HTML code
	// like doctype and opening html and body tags.
	include('./templates/header.inc');

	// File database.php includes function for
	// quering the database
	include('./data/database.php');
	
	if (!isset($_SESSION['user'])) { // The user must not be logged in
		if (isset($_POST['register'])) { // The form was submitted
			// Build the query string to insert the customer's data
			// TODO: Watch out! Protect this code against XSS!
			$query_string  = "INSERT INTO `customers` (`id`, `password`, `first_name`, `last_name`, `address`, `city`, `state`, `zip`, `phone`, `email`) ";
			$query_string .= "VALUES ('{$_POST['user_id']}', '{$_POST['password']}', '{$_POST['first_name']}', '{$_POST['last_name']}', '{$_POST['address1']}', '{$_POST['city']}', '{$_POST['state']}', '{$_POST['zip']}', '{$_POST['phone']}', '{$_POST['email']}')";
				
			// Execute the query
			query($query_string);
		
			// Stores user data in the session
			$_SESSION['user'] = array('id' => $_POST['user_id'], 'name' => $_POST['first_name']);
		
			// Adds a javascript that redirects to the products page
			print '<script type="text/javascript">window.location.href = "products.php"</script>';
		}
	} else { // If it is then we redirect it
		// Adds a javascript that redirects to the user details page
		print '<script type="text/javascript">window.location.href = "user_details.php"</script>';
	}
?>

	<div id="error-messages">
		<!-- The Javascript validation function will put the error messages here -->
	</div>
	<form id="user-form" action="registration.php" method="post" onsubmit="return validateUser()">
		<div class="form-item">
			<label for="user-user-id">User ID*</label>
			<input name="user_id" id="user-user-id" type="text" />
		</div>
		<div class="form-item">
			<label for="user-password">Password*</label>
			<input name="password" id="user-password" type="password" />
		</div>
		<div class="form-item">
			<label for="user-password-confirmation">Confirm password*</label>
			<input name="password_confirmation" id="user-password-confirmation" type="password" />
		</div>
		<div class="form-item">
			<label for="user-first-name">First name</label>
			<input name="first_name" id="user-first-name" type="text" />
		</div>
		<div class="form-item">
			<label for="user-last-name">Last name</label>
			<input name="last_name" id="user-last-name" type="text" />
		</div>
		<div class="form-item">
			<label for="user-address-1">Address line 1</label>
			<input name="address1" id="user-address-1" type="text" />
		</div>
		<div class="form-item">
			<label for="user-address-2">
				Address line 2
			</label>
			<span class="note">(optional)</span>
			<input name="address2" id="user-address-2" type="text" />
		</div>
		<div class="form-item">
			<label for="user-city">City/State</label>
			<input name="city" id="user-city" type="text" />
			<select name="state" id="user-state">
				<option value="NA">N/A</option>
				<option value="AL">AL</option> 
				<option value="AK">AK</option> 
				<option value="AZ">AZ</option> 
				<option value="AR">AR</option> 
				<option value="CA">CA</option> 
				<option value="CO">CO</option> 
				<option value="CT">CT</option> 
				<option value="DE">DE</option> 
				<option value="DC">DC</option> 
				<option value="FL">FL</option> 
				<option value="GA">GA</option> 
				<option value="HI">HI</option> 
				<option value="ID">ID</option> 
				<option value="IL">IL</option> 
				<option value="IN">IN</option> 
				<option value="IA">IA</option> 
				<option value="KS">KS</option> 
				<option value="KY">KY</option> 
				<option value="LA">LA</option> 
				<option value="ME">ME</option> 
				<option value="MD">MD</option> 
				<option value="MA">MA</option> 
				<option value="MI">MI</option> 
				<option value="MN">MN</option> 
				<option value="MS">MS</option> 
				<option value="MO">MO</option> 
				<option value="MT">MT</option> 
				<option value="NE">NE</option> 
				<option value="NV">NV</option> 
				<option value="NH">NH</option> 
				<option value="NJ">NJ</option> 
				<option value="NM">NM</option> 
				<option value="NY">NY</option> 
				<option value="NC">NC</option> 
				<option value="ND">ND</option> 
				<option value="OH">OH</option> 
				<option value="OK">OK</option> 
				<option value="OR">OR</option> 
				<option value="PA">PA</option> 
				<option value="PR">PR</option> 
				<option value="RI">RI</option> 
				<option value="SC">SC</option> 
				<option value="SD">SD</option> 
				<option value="TN">TN</option> 
				<option value="TX">TX</option> 
				<option value="UT">UT</option> 
				<option value="VT">VT</option> 
				<option value="VA">VA</option> 
				<option value="WA">WA</option> 
				<option value="WV">WV</option> 
				<option value="WI">WI</option> 
				<option value="WY">WY</option>
			</select>
		</div>
		<div class="form-item">
			<label for="user-zip">Zip code</label>
			<input name="zip" id="user-zip" type="text" />
		</div>
		<div class="form-item">
			<label for="user-phone">Telephone</label>
			<span class="note">555-555-1234</span>
			<input name="phone" id="user-phone" type="text" />
		</div>
		<div class="form-item">
			<label for="user-email">Email</label>
			<input name="email" id="user-email" type="text" />
		</div>
		<div class="form-submit">
			<input name="register" id="user-register" type="submit" value="Register" />
		</div>
	</form>
	
<?php

	// File footer.inc includes ending HTML code
	// like closing body and html tags.
	include('./templates/footer.inc');

?>