<?php

// Destroy the session
session_start();
unset($_SESSION['user']);

// ... and redirect to the products page
header("Location: products.php");