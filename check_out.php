<?php

	// File header.inc includes starting HTML code
	// like doctype and opening html and body tags.
	include('./templates/header.inc');

	// File database.php includes function for
	// quering the database
	include('./data/database.php');
	
	if (isset($_SESSION['user'])) { // Require the user to be logged in
		// Get the customer data from the database to fill the form with current values
		$customer = query("SELECT * FROM `customers` WHERE `id` = '{$_SESSION['user']['id']}'");
		$customer = $customer[0]; // We need just the first row
	} else { // ... if is not logged in then redirect it
		// Adds a javascript that redirects to the login page
		print '<script type="text/javascript">window.location.href = "login.php"</script>';
	}
?>

	<div id="checkout" class="page">
		<h1 id="checkout-order-summary-title">
			Your order summary
		</h1>
		<div id="checkout-order-summary">
			<?php
			
				// Query the database for the content of the cart, joining it with
				// the products table to get the details.
				$products = query("SELECT p.`id`, p.`price`, p.`detail`, c.`product_quantity` FROM `cart` c, `products` p WHERE c.`product_id` = p.`id`");
				
				if (!empty($products)) {
					// Output cart content table
				
					print '<form id="shopping-cart-update-form" action="shopping_cart.php" method="get">';
					print '<table id="cart-table">';
					print '<tr>';
					print '<th>Detail</th>';
					print '<th>Price</th>';
					print '<th>Quantity</th>';
					print '<th>Total</th>';
					print '</tr>';
				
					// Print each product
					for ($i = 0; $i != count($products); $i++) {
						$product = $products[$i];
				
						print '<tr id="' . $product["id"] . '">';
						print '<td>' . $product["detail"] . '</td>';
						print '<td>$' . $product["price"] . '</td>';
						print '<td>';
						print '<input id="catalog-quantity-' . $product["id"] . '" name="quantity_' . $product["id"] . '" type="text" value="' . $product["product_quantity"] . '" />';
						print '<input id="update-"' . $product["id"] . ' name="update" type="hidden" value="' . $product["id"] . '" />';
						print '<a href="javascript:document.forms[\'shopping-cart-update-form\'].submit()">Update</a> ';
						print '<a href="shopping_cart.php?remove=' . $product["id"] . '">Remove</a>';
						print '</td>';
						print '<td>$' . ($product["price"] * $product["product_quantity"]) . '</td>';
						print '</tr>';
					}
					print '</table>';
					print '</form>';
				} else {
					print '<p>The cart is empty</p>';
				}
			?>
		</div>
		
		<div id="checkout-billing-info">
			<div id="error-messages">
				<!-- The Javascript validation function will put the error messages here -->
			</div>
			<form id="checkout-billing-info-form" action="order_details.php" method="post" onsubmit="return validateCheckout()">
				<div class="form-item">
					<label for="checkout-country">Country</label>
					<select id="checkout-country" name="country">
						<option value="United States">United States</option>
						<option selected="selected" value="Puerto Rico">Puerto Rico</option>
					</select>
				</div>
				<div class="form-item">
					<label for="checkout-card-number">Card number</label>
					<input id="checkout-card-number" name="card_number" type="text" />
				</div>
				<div class="form-item">
					<label for="checkout-payment-types">Payment Types</label>
					<div id="checkout-payment-types">
						<ul>
							<li id="checkout-payment-type-visa"><img src="images/cards/visa.gif" /></li>
							<li id="checkout-payment-type-mastercard"><img src="images/cards/mastercard.gif" /></li>
							<li id="checkout-payment-type-discover"><img src="images/cards/discover.gif" /></li>
							<li id="checkout-payment-type-amex"><img src="images/cards/amex.gif" /></li>
						</ul>
					</div>
				</div>
				<div class="form-item">
					<label for="checkout-expiration-date">Expiration date</label>
					<div id="checkout-expiration-date">
						<div class="inline">
							<label for="checkout-expiration-date-month" class="note">mm</label>
							<input id="checkout-expiration-date-month" name="expiration_date_month" type="text" />
						</div>
						<span> / </span>
						<div class="inline">
							<label for="checkout-expiration-date-year" class="note">yy</label>
							<input id="checkout-expiration-date-year" name="expiration_date_year" type="text" />
						</div>
					</div>
				</div>
				<div class="form-item">
					<label for="checkout-csc">CSC</label>
					<input id="checkout-csc" name="csc" type="text" />
				</div>
				<div class="form-item">
					<label for="checkout-first-name">First name</label>
					<input id="checkout-first-name" name="first_name" type="text" value="<?php print $customer['first_name'] ?>" />
				</div>
				<div class="form-item">
					<label for="checkout-last-name">Last name</label>
					<input id="checkout-last-name" name="last_name" type="text" value="<?php print $customer['last_name'] ?>" />
				</div>
				<div class="form-item">
					<label for="checkout-address-1">Address line 1</label>
					<input id="checkout-address-1" name="address" type="text" value="<?php print $customer['address'] ?>" />
				</div>
				<div class="form-item">
					<label for="checkout-address-2">
						Address line 2
					</label>
					<span class="note">(optional)</span>
					<input id="checkout-address-2" name="address2" type="text" />
				</div>
				<div class="form-item">
					<label for="checkout-city">City/State</label>
					<input id="checkout-city" name="city" type="text" value="<?php print $customer['city'] ?>" />
					<select id="checkout-state" name="state">
						<option <?php if ($customer['state'] == 'NA') { print 'selected="selected"'; } ?> value="NA">N/A</option>
						<option <?php if ($customer['state'] == 'AL') { print 'selected="selected"'; } ?> value="AL">AL</option> 
						<option <?php if ($customer['state'] == 'AK') { print 'selected="selected"'; } ?> value="AK">AK</option> 
						<option <?php if ($customer['state'] == 'AZ') { print 'selected="selected"'; } ?> value="AZ">AZ</option> 
						<option <?php if ($customer['state'] == 'AR') { print 'selected="selected"'; } ?> value="AR">AR</option> 
						<option <?php if ($customer['state'] == 'CA') { print 'selected="selected"'; } ?> value="CA">CA</option> 
						<option <?php if ($customer['state'] == 'CO') { print 'selected="selected"'; } ?> value="CO">CO</option> 
						<option <?php if ($customer['state'] == 'CT') { print 'selected="selected"'; } ?> value="CT">CT</option> 
						<option <?php if ($customer['state'] == 'DE') { print 'selected="selected"'; } ?> value="DE">DE</option> 
						<option <?php if ($customer['state'] == 'DC') { print 'selected="selected"'; } ?> value="DC">DC</option> 
						<option <?php if ($customer['state'] == 'FL') { print 'selected="selected"'; } ?> value="FL">FL</option> 
						<option <?php if ($customer['state'] == 'GA') { print 'selected="selected"'; } ?> value="GA">GA</option> 
						<option <?php if ($customer['state'] == 'HI') { print 'selected="selected"'; } ?> value="HI">HI</option> 
						<option <?php if ($customer['state'] == 'ID') { print 'selected="selected"'; } ?> value="ID">ID</option> 
						<option <?php if ($customer['state'] == 'IL') { print 'selected="selected"'; } ?> value="IL">IL</option> 
						<option <?php if ($customer['state'] == 'IN') { print 'selected="selected"'; } ?> value="IN">IN</option> 
						<option <?php if ($customer['state'] == 'IA') { print 'selected="selected"'; } ?> value="IA">IA</option> 
						<option <?php if ($customer['state'] == 'KS') { print 'selected="selected"'; } ?> value="KS">KS</option> 
						<option <?php if ($customer['state'] == 'KY') { print 'selected="selected"'; } ?> value="KY">KY</option> 
						<option <?php if ($customer['state'] == 'LA') { print 'selected="selected"'; } ?> value="LA">LA</option> 
						<option <?php if ($customer['state'] == 'ME') { print 'selected="selected"'; } ?> value="ME">ME</option> 
						<option <?php if ($customer['state'] == 'MD') { print 'selected="selected"'; } ?> value="MD">MD</option> 
						<option <?php if ($customer['state'] == 'MA') { print 'selected="selected"'; } ?> value="MA">MA</option> 
						<option <?php if ($customer['state'] == 'MI') { print 'selected="selected"'; } ?> value="MI">MI</option> 
						<option <?php if ($customer['state'] == 'MN') { print 'selected="selected"'; } ?> value="MN">MN</option> 
						<option <?php if ($customer['state'] == 'MS') { print 'selected="selected"'; } ?> value="MS">MS</option> 
						<option <?php if ($customer['state'] == 'MO') { print 'selected="selected"'; } ?> value="MO">MO</option> 
						<option <?php if ($customer['state'] == 'MT') { print 'selected="selected"'; } ?> value="MT">MT</option> 
						<option <?php if ($customer['state'] == 'NE') { print 'selected="selected"'; } ?> value="NE">NE</option> 
						<option <?php if ($customer['state'] == 'NV') { print 'selected="selected"'; } ?> value="NV">NV</option> 
						<option <?php if ($customer['state'] == 'NH') { print 'selected="selected"'; } ?> value="NH">NH</option> 
						<option <?php if ($customer['state'] == 'NJ') { print 'selected="selected"'; } ?> value="NJ">NJ</option> 
						<option <?php if ($customer['state'] == 'NM') { print 'selected="selected"'; } ?> value="NM">NM</option> 
						<option <?php if ($customer['state'] == 'NY') { print 'selected="selected"'; } ?> value="NY">NY</option> 
						<option <?php if ($customer['state'] == 'NC') { print 'selected="selected"'; } ?> value="NC">NC</option> 
						<option <?php if ($customer['state'] == 'ND') { print 'selected="selected"'; } ?> value="ND">ND</option> 
						<option <?php if ($customer['state'] == 'OH') { print 'selected="selected"'; } ?> value="OH">OH</option> 
						<option <?php if ($customer['state'] == 'OK') { print 'selected="selected"'; } ?> value="OK">OK</option> 
						<option <?php if ($customer['state'] == 'OR') { print 'selected="selected"'; } ?> value="OR">OR</option> 
						<option <?php if ($customer['state'] == 'PA') { print 'selected="selected"'; } ?> value="PA">PA</option> 
						<option <?php if ($customer['state'] == 'PR') { print 'selected="selected"'; } ?> value="PR">PR</option> 
						<option <?php if ($customer['state'] == 'RI') { print 'selected="selected"'; } ?> value="RI">RI</option> 
						<option <?php if ($customer['state'] == 'SC') { print 'selected="selected"'; } ?> value="SC">SC</option> 
						<option <?php if ($customer['state'] == 'SD') { print 'selected="selected"'; } ?> value="SD">SD</option> 
						<option <?php if ($customer['state'] == 'TN') { print 'selected="selected"'; } ?> value="TN">TN</option> 
						<option <?php if ($customer['state'] == 'TX') { print 'selected="selected"'; } ?> value="TX">TX</option> 
						<option <?php if ($customer['state'] == 'UT') { print 'selected="selected"'; } ?> value="UT">UT</option> 
						<option <?php if ($customer['state'] == 'VT') { print 'selected="selected"'; } ?> value="VT">VT</option> 
						<option <?php if ($customer['state'] == 'VA') { print 'selected="selected"'; } ?> value="VA">VA</option> 
						<option <?php if ($customer['state'] == 'WA') { print 'selected="selected"'; } ?> value="WA">WA</option> 
						<option <?php if ($customer['state'] == 'WV') { print 'selected="selected"'; } ?> value="WV">WV</option> 
						<option <?php if ($customer['state'] == 'WI') { print 'selected="selected"'; } ?> value="WI">WI</option> 
						<option <?php if ($customer['state'] == 'WY') { print 'selected="selected"'; } ?> value="WY">WY</option>
					</select>
				</div>
				<div class="form-item">
					<label for="checkout-zip">Zip code</label>
					<input id="checkout-zip" name="zip" type="text" value="<?php print $customer['zip'] ?>" />
				</div>
				<div class="form-item">
					<label for="checkout-phone">Telephone</label>
					<span class="note">555-555-1234</span>
					<input id="checkout-phone" name="phone" type="text" value="<?php print $customer['phone'] ?>" />
				</div>
				<div class="form-item">
					<label for="checkout-email">Email</label>
					<input id="checkout-email" name="email" type="text" value="<?php print $customer['email'] ?>" />
				</div>
				<div class="form-submit">
					<input id="checkout-review-and-continue" name="review_and_continue" type="submit" value="Review and Continue" />
				</div>
			</form>
		</div>
	</div>
	
<?php

	// File footer.inc includes ending HTML code
	// like closing body and html tags.
	include('./templates/footer.inc');

?>